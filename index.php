<?php
$alert='';
session_start();

if(!empty($_SESSION['active']))
{
    header('location: proyecto/');
}
else{
if(!empty($_POST))
{
	if(empty($_POST['correo'])||empty($_POST['clave'])){
        $alert='Ingrese su correo y su clave';
    }else{
        require_once "conexion_bd.php";

        $user=mysqli_real_escape_string($conn,$_POST['correo']);
        $pass=md5(mysqli_real_escape_string($conn,$_POST['clave']));

        $query=mysqli_query($conn,"SELECT * FROM mcastillo.usuarioss WHERE correo='$user' AND clave='$pass'");
        mysqli_close($conn);
        $result=mysqli_num_rows($query);

        if($result > 0){
            $data=mysqli_fetch_array($query);
            $_SESSION['active']=true;
            $_SESSION['idUser']=$data['idusuario'];
            $_SESSION['user']=$data['correo'];
            $_SESSION['rol']=$data['rol'];
            $_SESSION['fecha']=$data['fecha'];

            header('location: proyecto/');
        }
        else{
            $alert="El usuario o la clave son incorrectos";
            session_destroy();
        }
    }
}
} 
?> 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login | Conferencias</title>
    <link rel="stylesheet" type="text/css" href="proyecto/css/styles.css">
</head>
<body>
    <section id="container">
       <form action="" method="post">
            <img src="proyecto/imagenes/login.jpg" alt="Login">
            <input type="email" name="correo" placeholder="Correo">
            <input type="password" name="clave" placeholder="Contraseña">
            <div class="alert"> <?php echo isset($alert) ? $alert : '';?></div>
            <input type="submit" value="INGRESAR">
            <button type="button" class="btn submits sign-up"><a href="nuevousuario.php" style="color: white">REGISTRAR USUARIO</a></button>
       </form>
    </section>
</body>
</html>