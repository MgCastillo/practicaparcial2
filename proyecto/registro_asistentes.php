<?php include("../conexion_bd.php"); ?>
<?php include('includes/header.php'); ?>

<br>
     <div class="modal-footer">
       <?php if (isset($_SESSION['message'])) { ?>
        <div class="alert alert-<?= $_SESSION['message_type']?> alert-dismissible fade show" role="alert">
          <?= $_SESSION['message']?>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
       <?php session_unset(); } ?>

        <form class="d-flex" action="" method="get">
            <input class="form-control me-sm-2" type="text" name="busqueda" id="busqueda" placeholder="Buscar">
            <button class="btn btn-secondary my-2 my-sm-0" name="enviar" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
        </form>
     </div>

<main class="container p-3">
  <div class="row">
    <div class="col-md-2.5">
      <!-- MESSAGES -->

      <?php if (isset($_SESSION['message'])) { ?>
      <div class="alert alert-<?= $_SESSION['message_type']?> alert-dismissible fade show" role="alert">
        <?= $_SESSION['message']?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <?php session_unset(); } ?>

      <!-- ADD TASK FORM -->
      <div class="card card-body">
        <form action="guardar.php" method="POST">
          <button type="button" class="btn btn-info" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Registro Asistentes</button>

          <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ingresar Asistentes</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form>
                  <div class="form-group">
                    <input type="text" name="nombre" class="form-control" placeholder="Nombres" required/>
                  </div>
                  <div class="form-group">
                    <input type="text" name="apellidos" class="form-control" placeholder="Apellidos" required/>
                  </div>
                  <div class="form-group">
                    <input type="email" name="correo" class="form-control" placeholder="Correo Electronico" required/>
                  </div>
                </form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>                
                <button type="submit" name="asistentes" class="btn btn-info">Guardar</button>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>

  <div class="col-md-16">
    <table class="table table-bordered">
      <thead>
        <tr>
          <th>No.</th>
          <th>Nombres</th>
          <th>Apellidos</th>
          <th>Correo</th>
          <th>Opciones</th>
        </tr>
      </thead>
      <tbody>


    <?php
        if(isset($_GET['enviar'])){
            $busqueda=$_GET['busqueda'];

            $consulta=$conn->query("SELECT * FROM mcastillo.asistentes WHERE (id_asistentes LIKE '%$busqueda%' OR
									nombre LIKE '%$busqueda%' OR
									apellidos LIKE '%$busqueda%' OR
									correo LIKE '%$busqueda%')
									;");

            while($row = $consulta->fetch_array()){?>
            <tr>
              <td><?php echo $row['id_asistentes']; ?></td>          
              <td><?php echo $row['nombre']; ?></td>
              <td><?php echo $row['apellidos']; ?></td>
              <td><?php echo $row['correo']; ?></td>
              <td>
                <a href="editar_asistentes.php?id_asistentes=<?php echo $row['id_asistentes']?>" class="btn btn-primary">
                  <i class="fas fa-marker"></i>
                </a>
                <a href="eliminar.php?id_asistentes=<?php echo $row['id_asistentes']?>" class="btn btn-warning">
                  <i class="far fa-trash-alt"></i>
                </a>
                <a href="consultar_asistentes.php?id_asistentes=<?php echo $row['id_asistentes']?>" class="btn btn-success">
                  <i class="far fa-eye"></i>
                </a>              
              </td>
            </tr>
            <?php }
            }else{ ?>
      <?php        
        $sql = "SELECT * FROM mcastillo.asistentes;";
        $result = $conn->query($sql);

        if ($result->num_rows > 0) {
          while($row = $result->fetch_assoc()) { ?>
            <tr>
              <td><?php echo $row['id_asistentes']; ?></td>          
              <td><?php echo $row['nombre']; ?></td>
              <td><?php echo $row['apellidos']; ?></td>
              <td><?php echo $row['correo']; ?></td>
              <td>
                <a href="editar_asistentes.php?id_asistentes=<?php echo $row['id_asistentes']?>" class="btn btn-primary">
                  <i class="fas fa-marker"></i>
                </a>
                <a href="eliminar.php?id_asistentes=<?php echo $row['id_asistentes']?>" class="btn btn-warning">
                  <i class="far fa-trash-alt"></i>
                </a>
                <a href="consultar_asistentes.php?id_asistentes=<?php echo $row['id_asistentes']?>" class="btn btn-success">
                  <i class="far fa-eye"></i>
                </a>              
              </td>
            </tr>
      <?php }
          } else {
            echo "Aun no hay registros";
          }
	} ?>

      
      </tbody>
    </table>
  </div>
  </div>
</main>

<?php include('includes/footer.php'); ?>