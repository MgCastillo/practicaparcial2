<?php include("../conexion_bd.php"); ?>
<?php include('includes/header.php'); ?>

<br>
     <div class="modal-footer">
       <?php if (isset($_SESSION['message'])) { ?>
        <div class="alert alert-<?= $_SESSION['message_type']?> alert-dismissible fade show" role="alert">
          <?= $_SESSION['message']?>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
       <?php session_unset(); } ?>

	<form class="d-flex" action="" method="get">
		<input class="form-control me-sm-2" type="text" name="busqueda" id="busqueda" placeholder="Buscar">
		<button class="btn btn-secondary my-2 my-sm-0" name="enviar" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
        </form>
    </div>

<main class="container p-2">

  <div class="row">
    <div class="col-md-2.5">
      <!-- MESSAGES -->

      <!-- ADD TASK FORM -->
      <div class="card card-body">

      <form action="guardar.php" method="POST" id="f1" name="f1">

          <button type="button" class="btn btn-info" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Ingresar Registros</button>
        
          <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Registro de Conferencistas</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form>
                  <div class="form-group">
                    <input type="text" name="nombre" class="form-control" placeholder="Ingrese nombres" required/>
                  </div>
                  <div class="form-group">
                    <input type="text" name="apellidos" class="form-control" placeholder="Ingrese apellidos" required/>
                  </div>
                  <div class="form-group">
                    <input type="text" name="profesion" class="form-control" placeholder="Ingrese Profesion" required/>
                  </div>
                  <div class="form-group">
                    <input type="text" name="biografia" class="form-control" placeholder="Biografia una breve descripcion" required/>
                  </div>
                  <div class="form-group">
                    <input type="email" name="correo" class="form-control" placeholder="Ingrese Correo electronico" required/>
                  </div>
                </form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>
                <button type="submit" name="conferencistas" class="btn btn-info">Guardar Registro</button>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>

    <div class="col-md-18">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>Nombres</th>
            <th>Apellidos</th>
            <th>Profesion</th>
            <th>Biografia</th>
            <th>Correo</th>
            <th>Opciones</th>
          </tr>
        </thead>
        <tbody>


    <?php
        if(isset($_GET['enviar'])){
            $busqueda=$_GET['busqueda'];

            $consulta=$conn->query("SELECT * FROM mcastillo.conferencistas WHERE ( nombre LIKE '%$busqueda%' OR
										apellidos LIKE '%$busqueda%' OR
										profesion LIKE '%$busqueda%' OR
										biografia LIKE '%$busqueda%' OR
										correo LIKE '%$busqueda%')
										;");

            while($row = $consulta->fetch_array()){?>
                <tr>
                    <td><?php echo $row['nombre']; ?></td>
                    <td><?php echo $row['apellidos']; ?></td>
                    <td><?php echo $row['profesion']; ?></td>
                    <td><?php echo $row['biografia']; ?></td>
                    <td><?php echo $row['correo']; ?></td>
                    <td>
                        <a href="editar_conferencistas.php?id_conferencistas=<?php echo $row['id_conferencistas']?>" class="btn btn-primary">
                            <i class="fas fa-marker"></i>
                        </a>
                        <a href="eliminar.php?id_conferencistas=<?php echo $row['id_conferencistas']?>" class="btn btn-warning">
                            <i class="far fa-trash-alt"></i>
                        </a>
                        <a href="consultar_conferencistas.php?id_conferencistas=<?php echo $row['id_conferencistas']?>" class="btn btn-success">
                            <i class="far fa-eye"></i>
                        </a>              
                    </td>
                </tr>
            <?php }
            }else{ ?>
          <?php
          $sql = "SELECT * FROM mcastillo.conferencistas;";
          $result = $conn->query($sql);

          if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) { ?>
              <tr>    
                <td><?php echo $row['nombre']; ?></td>
                <td><?php echo $row['apellidos']; ?></td>
                <td><?php echo $row['profesion']; ?></td>
                <td><?php echo $row['biografia']; ?></td>
                <td><?php echo $row['correo']; ?></td>
                <td>
                  <a href="editar_conferencistas.php?id_conferencistas=<?php echo $row['id_conferencistas']?>" class="btn btn-primary">
                    <i class="fas fa-marker"></i>
                  </a>
                  <a href="eliminar.php?id_conferencistas=<?php echo $row['id_conferencistas']?>" class="btn btn-warning">
                    <i class="far fa-trash-alt"></i>
                  </a>
                  <a href="consultar_conferencistas.php?id_conferencistas=<?php echo $row['id_conferencistas']?>" class="btn btn-success">
                    <i class="far fa-eye"></i>
                  </a>              
                </td>
              </tr>
        <?php }
          } else {
            echo "Aun no hay registros";
          }
	} ?>

        </tbody>
      </table>
    </div>
  </div>
</main>

<?php include('includes/footer.php'); ?>
