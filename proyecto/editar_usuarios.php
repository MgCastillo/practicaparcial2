<?php
include("../conexion_bd.php");

//-------------------------------------------------------
if  (isset($_GET['idusuario'])) {
  $idusuario = $_GET['idusuario'];
  $sql = "SELECT * FROM mcastillo.usuarioss WHERE idusuario= '$idusuario'";
  $result = mysqli_query($conn, $sql);
  
  if (mysqli_num_rows($result) == 1) {
    $row = mysqli_fetch_array($result);
    $correo = $row['correo'];
    $clave = $row['clave'];
    $rol = $row['rol'];
    $fecha = $row['fecha'];
  }
}

if (isset($_POST['update'])) {
  $idusuario= $_GET['idusuario'];
  $correo = $_POST['correo'];
  $clave = $_POST['clave'];
  $rol = $_POST['rol'];
  $fecha = $row['fecha'];

  $sql = "UPDATE mcastillo.usuarioss set correo = '$correo', clave = md5('$clave'), rol = '$rol', fecha = '$fecha' WHERE idusuario = '$idusuario'";
  mysqli_query($conn, $sql);
  
  $_SESSION['message'] = 'Modificado Correctamente';
  $_SESSION['message_type'] = 'primary';
  header('Location: registro_usuarios.php');
}

?>


<?php include('includes/header2.php'); ?>

<br>
<div class="container p-4">
  <div class="row">
    <div class="col-md-5 mx-auto">
      <div class="card card-body">
        <form action="editar_usuarios.php?idusuario=<?php echo $_GET['idusuario']; ?>" method="POST">
          <legend><strong>Datos del usuario</strong></legend>
          <div class="form-group">
            <label><strong>Correo Electronico</strong></label>
            <input type="email" name="correo" id="correo" class="form-control" placeholder="Correo electrónico" value="<?php echo $correo; ?>">
          </div>
          <div class="form-group">
            <label><strong>Contraseña</strong></label>
            <input type="password" name="clave" id="clave" class="form-control" placeholder="Ingrese su contraseña" value="<?php echo $clave; ?>">
          </div>
          <div class="form-group">
            <label><strong>Rol</strong></label>
            <select name="rol" id="rol" class="form-control">
              <option value="0">Seleccione:</option>
                <?php
                  $query = $conn -> query ("SELECT * FROM mcastillo.rol");
                  while ($rol = mysqli_fetch_array($query)) {
                      echo '<option value="'.$rol[idrol].'">'.$rol[rol].'</option>';
                  }
                ?>
            </select>
          </div>
          <div class="form-group">
            <label><strong>Fecha de creacion</strong></label>
            <input readonly type="date" name="fecha" id="fecha" class="form-control" value="<?php echo $fecha; ?>" required/>
         </div>

        <a href="registro_usuarios.php" class="btn btn-warning">Regresar</a>
        <button class="btn btn-primary" name="update"> Modificar </button>
      </form>
      </div>
    </div>
  </div>
</div>

<?php include('includes/footer.php'); ?>