<?php
session_start();
    if($_SESSION['rol']!=1){
       header('Location: ./');
   }
 ?>

<?php include("../conexion_bd.php"); ?>
<?php include('includes/header.php'); ?>

<br>
     <div class="modal-footer">
       <?php if (isset($_SESSION['message'])) { ?>
        <div class="alert alert-<?= $_SESSION['message_type']?> alert-dismissible fade show" role="alert">
          <?= $_SESSION['message']?>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
       <?php session_unset(); } ?>

	<form class="d-flex" action="" method="get">
		<input class="form-control me-sm-2" type="text" name="busqueda" id="busqueda" placeholder="Buscar">
		<button class="btn btn-secondary my-2 my-sm-0" name="enviar" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
        </form>
    </div>

<main class="container p-3">

  <div class="row">
    <div class="col-md-2.5">
      <!-- MESSAGES -->

      <!-- ADD TASK FORM -->
      <div class="card card-body">

      <form action="guardar.php" method="POST">

          <button type="button" class="btn btn-info" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Ingresar Registros</button>
        
          <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Registro de Usuarios</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form>
                  <div class="form-group">
                    <input type="email" name="correo" id="correo" class="form-control" placeholder="Correo electrónico" required>
                  </div>
                  <div class="form-group">
                    <input type="password" name="clave" id="clave" class="form-control" placeholder="Ingrese su contraseña" required>
                  </div>
                  <div class="form-group">
		                  <input readonly type="date" id="fecha" name="fecha" class="form-control" value="<?php echo date('Y-m-d'); ?>" />
                  </div>
                  <div class="form-group">
                    <select name="rol" id="rol" class="form-control">
                      <option value="0">Seleccione:</option>
                        <?php
                          $query = $conn -> query ("SELECT * FROM mcastillo.rol");
                          while ($rol = mysqli_fetch_array($query)) {
                              echo '<option value="'.$rol[idrol].'">'.$rol[rol].'</option>';
                          }
                        ?>
                    </select>
                  </div>
                </form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>
                <button type="submit" name="usuarios" class="btn btn-info">Guardar Registro</button>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>

    <div class="col-md-9">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>Correo Electronico</th>
            <th>Rol</th>
            <th>Fecha</th>
            <th>Opciones</th>
          </tr>
        </thead>
        <tbody>


    <?php
        if(isset($_GET['enviar'])){
            $busqueda=$_GET['busqueda'];

            $consulta=$conn->query("SELECT * FROM mcastillo.usuarioss WHERE (
										correo LIKE '%$busqueda%' OR
										rol LIKE '%$busqueda%' OR
										fecha LIKE '%$busqueda%')
										;");

            while($row = $consulta->fetch_array()){?>
                <tr>
                    <td><?php echo $row['correo'];  ?></td>
                    <td><?php echo $row['rol'];     ?></td>
                    <td><?php echo $row['fecha'];   ?></td>
                    <td>
                        <a href="editar_usuarios.php?idusuario=<?php echo $row['idusuario']?>" class="btn btn-primary">
                            <i class="fas fa-marker"></i>
                        </a>
                        <a href="eliminar.php?idusuario=<?php echo $row['idusuario']?>" class="btn btn-warning">
                            <i class="far fa-trash-alt"></i>
                        </a>
                    </td>
                </tr>
            <?php }
            }else{ ?>
          <?php
          $sql = "SELECT * FROM mcastillo.usuarioss;";
          $result = $conn->query($sql);

          if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) { ?>
              <tr>          
                <td><?php echo $row['correo']; ?></td>
                <td><?php echo $row['rol']; ?></td>
                <td><?php echo $row['fecha']; ?></td>
                <td>
                  <a href="editar_usuarios.php?idusuario=<?php echo $row['idusuario']?>" class="btn btn-primary">
                    <i class="fas fa-marker"></i>
                  </a>
                  <a href="eliminar.php?idusuario=<?php echo $row['idusuario']?>" class="btn btn-warning">
                    <i class="far fa-trash-alt"></i>
                  </a>          
                </td>
              </tr>
        <?php }
          } else {
            echo "Aun no hay registros";
          }
	} ?>

        </tbody>
      </table>
    </div>
  </div>
</main>

<?php include('includes/footer.php'); ?>
