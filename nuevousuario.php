<?php
include("conexion_bd.php");
?>
    <title>Registro Usuario</title>
    <link rel="stylesheet" type="text/css" href="proyecto/css/styles.css">
    <link rel="stylesheet" type="text/css" href="proyecto/css/login.css">
    <script type="text/javascript" src="sistema/js/login.js"></script>

<div class="overlay">
    <!--ALERT-->
      <?php if (isset($_SESSION['message'])) { ?>
      <div class="alert alert-<?= $_SESSION['message_type']?> alert-dismissible fade show" role="alert">
        <?= $_SESSION['message']?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <?php session_unset(); } ?>

<form action="proyecto/guardar.php" method="POST">
   <div class="con">
   <header class="head-form">
      <button type="button" class="btn submit frgt-pass"><a href="index.php" style="text-decoration: none; color: black; font-family: Arial, Helvetica, sans-serif; display:flex; float: right">Ir al Inicio</a></button>
      <h3>Registrar Nuevo Usuario</h3>
   </header>
        <section id="container">
   <br>
   <div class="field-set">     
      <span class="input-item"><i class="fa fa-user-circle"></i></span>
      <input type="email" name="correo" id="correo" class="form-input" placeholder="Correo electronico" required>
      <br><br>
     
      <span class="input-item"><i class="fa fa-key"></i></span>
      <input class="form-input" type="password" placeholder="Contrase&ntilde;a" id="clave" name="clave" required>
      <br><br>

      <span class="input-item"><i class="fa fa-user-circle"></i></span>
      <input readonly type="date" name="fecha" id="fecha" class="form-input" value="<?php echo date('Y-m-d'); ?>">
      <br><br>

      <span class="select-item"><i class="fa fa-user-circle"></i></span>
      <select  name="rol" id="rol" class="form-control">
      
      <option value="0">Seleccione:</option>
                        <?php
                          $query = $conn -> query ("SELECT * FROM mcastillo.rol");
                          while ($rol = mysqli_fetch_array($query)) {
                              echo '<option value="'.$rol[idrol].'">'.$rol[rol].'</option>';
                          }
                        ?>
                        </select>
      <br><br>

      <button type="submit" name="usuarios">Guardar Registro de Usuario</button>
   </div>
                        </section>
  </div>

</form>
</div>