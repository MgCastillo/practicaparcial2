<?php
include("../conexion_bd.php");

if(isset($_GET['id_asistentes'])) {
  $id_asistentes = $_GET['id_asistentes'];
  $sql = "DELETE FROM mcastillo.asistentes WHERE id_asistentes = '$id_asistentes'";
  $result = mysqli_query($conn, $sql);
  if(!$result) {
    die("Query Failed.");
  }

  $_SESSION['message'] = 'Eliminado Correctamente';
  $_SESSION['message_type'] = 'warning';
  header('Location: registro_asistentes.php');
}


if(isset($_GET['id_conferencistas'])) {
  $id_conferencistas = $_GET['id_conferencistas'];
  $sql = "DELETE FROM mcastillo.conferencistas WHERE id_conferencistas = '$id_conferencistas'";
  $result = mysqli_query($conn, $sql);
  if(!$result) {
    die("Query Failed.");
  }

  $_SESSION['message'] = 'Eliminado Correctamente';
  $_SESSION['message_type'] = 'warning';
  header('Location: registro_conferencistas.php');
}


if(isset($_GET['id_conferencias'])) {
  $id_conferencias = $_GET['id_conferencias'];
  $sql = "DELETE FROM mcastillo.conferencias WHERE id_conferencias = '$id_conferencias'";
  $result = mysqli_query($conn, $sql);
  if(!$result) {
    die("Query Failed.");
  }

  $_SESSION['message'] = 'Eliminado Correctamente';
  $_SESSION['message_type'] = 'warning';
  header('Location: registro_conferencias.php');
}


if(isset($_GET['idusuario'])) {
  $idusuario = $_GET['idusuario'];
  $sql = "DELETE FROM mcastillo.usuarioss WHERE idusuario = '$idusuario'";
  $result = mysqli_query($conn, $sql);
  if(!$result) {
    die("Query Failed.");
  }

  $_SESSION['message'] = 'Eliminado Correctamente';
  $_SESSION['message_type'] = 'warning';
  header('Location: registro_usuarios.php');
}

?>