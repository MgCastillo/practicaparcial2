<?php
session_start();
  if(empty($_SESSION['active']))
 {
     header('location: ../');
  }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>Conferencias</title>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <!-- BOOTSTRAP 4 -->
    <link rel="stylesheet" href="https://bootswatch.com/4/sandstone/bootstrap.min.css">
    <!-- FONT AWESOEM -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
	<script type="text/javascript" src="js/jquery-3.4.1.js"></script>
	<script type="text/javascript" src="js/script.js"></script>
  </head>
  <body>

<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  <div class="container-fluid">
  
			<ul>
				<li><a href="index.php"><i class="fa fa-home" class="nav-item" aria-hidden="true"></i> Inicio</a></li>

			
				<li class="principal" class="nav-item">
					<a href="#" style="font-family: rockwell"><i class="fa fa-users" aria-hidden="true"></i> Usuarios</a>
					<ul>
						<li><a href="registro_usuarios.php"><i class="fa fa-user-plus" aria-hidden="true"></i> Registrar nuevo usuario</a></li>
					</ul>
				</li>

				

				
				<li class="principal" class="nav-item">
					<a href="#" style="font-family: rockwell"><i class="fa fa-users" aria-hidden="true"></i> Conferencistas</a>
					<ul>
						<li><a href="registro_conferencistas.php"><i class="fa fa-user-plus" aria-hidden="true"></i> Registrar Conferencista</a></li>
					</ul>
				</li>
				

				<li class="principal" class="nav-item">
					<a href="#"><i class="fa fa-list" aria-hidden="true"></i> Asistentes</a>
					<ul>
						<li><a href="registro_asistentes.php"><i class="fa fa-list" aria-hidden="true"></i> Registrar Asistentes</a></li>
					</ul>
				</li>
				
				<li class="principal" class="nav-item">
					<a href="#"><i class="fa fa-user-secret" aria-hidden="true"></i> Conferencias</a>
					<ul>
						<li><a href="registro_conferencias.php"><i class="fa fa-list" aria-hidden="true"></i>Registro Conferencias</a></li>
					</ul>
				</li>

			</ul>
      <a class="navbar-brand" href="#" style="font-family: 'GothamBook'; font-size: 14px; color: white; text-transform: none">
      <?php 
                    switch($_SESSION['rol']){
                    case 1:
                        $tipo_rol='Administrador';break;
                    case 2:
                        $tipo_rol='Supervisor';break;
                    }
                    echo $_SESSION['nombre']."(".$tipo_rol.")";
                    ?> </a>
 
    <a href="salir.php"><img class="close" src="imagenes/salir.png" alt="Salir del sistema" title="Salir"></a>

  </div>
</nav>

<div class="progress">
  <div class="progress-bar bg-warning" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
</div>