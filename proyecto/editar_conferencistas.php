<?php
include("../conexion_bd.php");

//-------------------------------------------------------
if  (isset($_GET['id_conferencistas'])) {
  $id_conferencistas = $_GET['id_conferencistas'];
  $sql = "SELECT * FROM mcastillo.conferencistas WHERE id_conferencistas= '$id_conferencistas'";
  $result = mysqli_query($conn, $sql);
  
  if (mysqli_num_rows($result) == 1) {
    $row = mysqli_fetch_array($result);
    $nombre = $row['nombre'];
    $apellidos = $row['apellidos'];
    $profesion = $row['profresion'];
    $biografia = $row['biografia'];
    $correo = $row['correo'];
  }
}

if (isset($_POST['update'])) {
  $id_conferencistas = $_GET['id_conferencistas'];
  $nombre = $_POST['nombre'];
  $apellidos = $_POST['apellidos'];
  $profesion = $_POST['profesion'];
  $biografia = $_POST['biografia'];
  $correo = $_POST['correo'];

  $sql = "UPDATE mcastillo.conferencistas set nombre = '$nombre', apellidos = '$apellidos', profesion = '$profesion' , biografia = '$biografia', correo = '$correo', WHERE id_conferencistas = '$id_conferencistas'";
  mysqli_query($conn, $sql);
  
  $_SESSION['message'] = 'Modificado Correctamente';
  $_SESSION['message_type'] = 'primary';
  header('Location: registro_conferencistas.php');
}

?>


<?php include('includes/header.php'); ?>

<br>
<div class="container p-4">
  <div class="row">
    <div class="col-md-5 mx-auto">
      <div class="card card-body">
        <form action="editar_pacientes.php?id_paciente=<?php echo $_GET['id_paciente']; ?>" method="POST" name="f1" id="f1">
          <legend><strong>Datos Conferencistas</strong></legend>
          <div class="form-group">
            <label><strong>Nombres</strong></label>
            <input type="text" name="nombre" class="form-control" placeholder="Nombres Conferencistas" value="<?php echo $nombre; ?>">
          </div>
          <div class="form-group">
            <label><strong>Apellidos</strong></label>
            <input type="text" name="apellidos" class="form-control" placeholder="Apellidos Conferencistas" value="<?php echo $apellidos; ?>">
          </div>
          <div class="form-group">
            <label><strong>Profesion</strong></label>
            <input type="text" name="profesion" class="form-control" placeholder="Profesion Conferencista" value="<?php echo $profesion; ?>">
          </div>
          <div class="form-group">
            <label><strong>Biografia</strong></label>
            <input type="text" name="biografia" class="form-control" placeholder="Biografia breve descripcion" value="<?php echo $biografia; ?>">
          </div>
          <div class="form-group">
            <label><strong>Correo</strong></label>
            <input type="email" name="correo" class="form-control" placeholder="Correo" value="<?php echo $correo; ?>">
          </div>        
        <a href="registro_conferencistas.php" class="btn btn-warning">Regresar</a>
        <button class="btn btn-primary" name="update"> Modificar </button>
      </form>
      </div>
    </div>
  </div>
</div>

<?php include('includes/footer.php'); ?>