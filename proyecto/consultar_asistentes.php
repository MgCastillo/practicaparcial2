<?php
session_start();
include("../conexion_bd.php");

//-----------------------------------------------------
if  (isset($_GET['id_asistentes'])) {
  $id_asistentes = $_GET['id_asistentes'];
  $query = "SELECT * FROM mcastillo.asistentes WHERE id_asistentes='$id_asistentes'";
  $result = mysqli_query($conn, $query);
  if (mysqli_num_rows($result) == 1) {
    $row = mysqli_fetch_array($result);
    $nombre = $row['nombre'];
    $apellidos = $row['apellidos'];
    $correo = $row['correo'];
  }
}
?>

<?php include('includes/header.php'); ?>

<br>
<div class="container p-4">
  <div class="row">
    <div class="col-md-6 mx-auto">
      <div class="card card-body">
        <form>
          <legend><strong>Datos Asistentes</strong></legend>
          <div class="form-group">
            <label><strong>Nombre</strong></label>
            <input readonly type="text" class="form-control" value="<?php echo $nombre; ?>">
          </div>
          <div class="form-group">
            <label><strong>Apellidos</strong></label>
            <input readonly type="text" class="form-control" value="<?php echo $apellidos; ?>">
          </div>
          <div class="form-group">
            <label><strong>Correo</strong></label>
            <input readonly type="email" class="form-control" value="<?php echo $correo; ?>">
          </div>
        </div>

        <div class="modal-footer">
          <a href="registro_asistentes.php" class="btn btn-warning">Regresar</a>
        </div>
	</form>
      </div>
    </div>
  </div>
</div>

<?php include('includes/footer.php'); ?>