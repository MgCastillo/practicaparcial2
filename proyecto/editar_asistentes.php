<?php
include("../conexion_bd.php");

//-------------------------------------------------------
if  (isset($_GET['id_asistentes'])) {
  $id_asistentes = $_GET['id_asistentes'];
  $sql = "SELECT * FROM mcastillo.asistentes WHERE id_asistentes = $id_asistentes;";
  $result = mysqli_query($conn, $sql);

  if (mysqli_num_rows($result) == 1) {
    $row = mysqli_fetch_array($result);
    $nombre = $row['nombre'];
    $apellidos = $row['apellidos'];
    $correo = $row['correo'];
  }
}


if (isset($_POST['update'])) {
  $id_asistentes = $_GET['id_asistentes'];
  $nombre = $_POST['nombre'];
  $apellidos = $_POST['apellidos'];
  $correo = $_POST['correo'];
  
  $sql = "UPDATE mcastillo.asistentes set nombre = '$nombre', apellidos = '$apellidos', correo = '$correo' WHERE id_asistentes = $id_asistentes;";
  mysqli_query($conn, $sql);

  $_SESSION['message'] = 'Modificado Correctamente';
  $_SESSION['message_type'] = 'primary';
  header('Location: registro_asistentes.php');
}
?>


<?php include('includes/header.php'); ?>

<br>
<div class="container p-4">
  <div class="row">
    <div class="col-md-4 mx-auto">
      <div class="card card-body">
      <form action="editar_asistentes.php?id_asistentes=<?php echo $_GET['id_asistentes']; ?>" method="POST">
        <legend><strong>Datos Asistentes</strong></legend>
        <div class="form-group">
          <label><strong>Nombres</strong></label>
          <input type="text" name="nombre" class="form-control" placeholder="Nombres" value="<?php echo $nombre; ?>" required/>
        </div>
        <div class="form-group">
          <label><strong>Apellidos</strong></label>
          <input type="text" name="apellidos" class="form-control" placeholder="Apellidos" value="<?php echo $apellidos; ?>" required/>
        </div>
        <div class="form-group">
          <label><strong>Correo</strong></label>          
          <input type="email" name="correo" class="form-control" placeholder="Correo Electronico" value="<?php echo $correo; ?>" required/>
        </div>
        <a href="registro_asistentes.php" class="btn btn-warning">Regresar</a>
        <button class="btn btn-primary" name="update"> Modificar </button>
      </form>
      </div>
    </div>
  </div>
</div>

<?php include('includes/footer.php'); ?>