<?php 
	date_default_timezone_set('America/Guatemala'); 
	
	function fechaC(){
		$mes = array("","Enero", 
					  "Febrero", 
					  "Marzo", 
					  "Abril", 
					  "Mayo", 
					  "Junio", 
					  "Julio", 
					  "Agosto", 
					  "Septiembre", 
					  "Octubre", 
					  "Noviembre", 
					  "Diciembre");
		$dia=array("Domingo","Lunes",
					  "Martes",
					  "Miércoles",
					  "Jueves",
					  "Viernes",
					  "Sábado");
	   

		return $dia[date('w')]." ".date('d')." de ". $mes[date('n')] . " de " . date('Y');
	}


 ?>