<?php
session_start();
include("../conexion_bd.php");

//-----------------------------------------------------
if  (isset($_GET['id_conferencias'])) {
  $id_conferencias = $_GET['id_conferencias'];
  $sql = "SELECT * FROM mcastillo.conferencias WHERE id_conferencias = '$id_conferencias'";
  $result = mysqli_query($conn, $sql);
  
  if (mysqli_num_rows($result) == 1) {
    $row = mysqli_fetch_array($result);
    $nombreconferencia = $row["nombreconferencia"];
    $conferencista = $row["conferencista"];
  }
}
?>

<?php include('includes/header.php'); ?>

<br>
<div class="container p-4">
  <div class="row">
    <div class="col-md-6 mx-auto">
      <div class="card card-body">
        <form>
          <legend><strong>Datos de Conferencias</strong></legend>
          <div class="form-group">
            <label><strong>Nombre Conferencia</strong></label>
            <input readonly type="text" class="form-control" value="<?php echo $nombreconferencia; ?>">
          </div>
          <div class="form-group">
            <label><strong>Conferencista</strong></label>
            <input readonly type="text" class="form-control" value="<?php echo $conferencista; ?>">
          </div>
          
        </div>

        <div class="modal-footer">
          <a href="registro_conferencias.php" class="btn btn-warning">Regresar</a>
        </div>
      </form>
      </div>
    </div>
  </div>
</div>

<?php include('includes/footer.php'); ?>