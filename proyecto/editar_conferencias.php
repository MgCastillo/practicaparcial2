<?php
include("../conexion_bd.php");

//-----------------------------------------------------
if  (isset($_GET['id_conferencias'])) {
  $id_conferencias = $_GET['id_conferencias'];
  $sql = "SELECT * FROM mcastillo.conferencias WHERE id_conferencias = '$id_conferencias'";
  $result = mysqli_query($conn, $sql);
  
  if (mysqli_num_rows($result) == 1) {
    $row = mysqli_fetch_array($result);
    $nombreconferencia = $row["nombreconferencia"];
    $conferencista = $row["conferencista"];
  }
}

if (isset($_POST['update'])) {
  $id_conferencias = $_GET['id_conferencias'];
  $nombreconferencia = $_POST["nombreconferencia"];
  $conferencista = $_POST["conferencista"];

  $sql = "UPDATE mcastillo.conferencias set nombreconferencia = '$nombreconferencia', conferencista = '$conferencista' WHERE id_conferencias = '$id_conferencias'";
  mysqli_query($conn, $sql);
  $_SESSION['message'] = 'Modificado Correctamente';
  $_SESSION['message_type'] = 'primary';
  header('Location: registro_conferencias.php');
}
?>

<?php include('includes/header.php'); ?>

<br>
<div class="container p-4">
  <div class="row">
    <div class="col-md-5 mx-auto">
      <div class="card card-body">
      <form action="editar_centros.php?id_centro=<?php echo $_GET['id_centro']; ?>" method="POST" name="f1" id="f1">
       	<legend><strong>Datos de Conferencias</strong></legend>
	<div class="form-group">
          <label><strong>Nombre Conferencia</strong></label>
          <input type="text" name="nombreconferencia" class="form-control" placeholder="Tema Conferencia" value="<?php echo $nombre_conferencia; ?>">
        </div>           
        <div class="form-group">
          <label><strong>Conferencista</strong></label>
          <input type="text" name="conferencista" class="form-control" placeholder="Nombre Conferencista" value="<?php echo $conferencista; ?>">
        </div>       
        <a href="registro_conferencias.php" class="btn btn-warning">Regresar</a>
        <button class="btn btn-primary" name="update"> Modificar </button>
      </form>
      </div>
    </div>
  </div>
</div>

<?php include('includes/footer.php'); ?>