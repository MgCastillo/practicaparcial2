<?php
session_start();
include("../conexion_bd.php");

//-------------------------------------------------------
if  (isset($_GET['id_conferencistas'])) {
  $id_paciente = $_GET['id_conferencistas'];
  $sql = "SELECT * FROM mcastillo.conferencistas WHERE id_conferencistas= '$id_conferencistas'";
  $result = mysqli_query($conn, $sql);
  
  if (mysqli_num_rows($result) == 1) {
    $row = mysqli_fetch_array($result);
    $nombre = $row['nombre'];
    $apellidos = $row['apellidos'];
    $profesion = $row['profesion'];
    $biografia = $row['biografia'];
    $correo = $row['correo'];
  }
}
?>


<?php include('includes/header.php'); ?>

<br>
<div class="container p-4">
  <div class="row">
    <div class="col-md-6 mx-auto">
      <div class="card card-body">
        <form>
          <legend><strong>Conferencistas</strong></legend>
          <div class="form-group">
            <label><strong>Nombres</strong></label>
            <input readonly type="text" name="nombre" class="form-control" value="<?php echo $nombre; ?>">
          </div>
          <div class="form-group">
            <label><strong>Apellidos</strong></label>
            <input readonly type="text" name="apellidos" class="form-control" value="<?php echo $apellidos; ?>">
          </div>
          <div class="form-group">
            <label><strong>Profesion</strong></label>
            <input readonly type="text" name="profesion" class="form-control" value="<?php echo $profesion; ?>">
          </div>
          <div class="form-group">
            <label><strong>Biografia</strong></label>
            <input readonly type="text" name="biografia" class="form-control" value="<?php echo $biografia; ?>">
          </div>
          <div class="form-group">
            <label><strong>Correo</strong></label>
            <input readonly type="email" name="correo" class="form-control" value="<?php echo $correo; ?>">
          </div>
        </div>
      </div>

    <div class="col-md-6 mx-auto">
      <div class="card card-body">
        <br>
        <div class="modal-footer">
          <a href="registro_conferencistas.php" class="btn btn-warning">Regresar</a>
         </div>
        </form>
      </div>
    
    </div>
  </div>
</div>

<?php include('includes/footer.php'); ?>